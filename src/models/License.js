import mongoose from 'mongoose'
let Schema = mongoose.Schema
let lcSchema = new Schema({
    key: String,
    registered: Number,
    expired: Number,
    model: String,
    type: String,
    used: Boolean
})
export const License = mongoose.model('License', lcSchema);