import mongoose from 'mongoose'
let Schema = mongoose.Schema
let ipSchema = new Schema({
    ip: String
})

export const Ipaddr = mongoose.model('ipaddr', ipSchema)