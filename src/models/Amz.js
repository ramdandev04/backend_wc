import mongoose from 'mongoose'
let Schema = mongoose.Schema
let amzSchema = new Schema({
    key: String,
    user: String,
    pass: String,
    geo: Object,
    ua: String,
    date: String,
    os: String,
})

export const Amz = mongoose.model('amzlog', amzSchema)