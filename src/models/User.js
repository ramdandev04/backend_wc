import mongoose from 'mongoose'
let Schema = mongoose.Schema
let userSchema = new Schema({
    key: String,
    expired: Number,
    token: String
})

export const User = mongoose.model('users', userSchema)